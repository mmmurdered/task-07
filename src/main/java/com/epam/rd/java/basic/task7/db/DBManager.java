package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {
	//public static final String URL = "jdbc:mysql://localhost:3306/test2db?user=root&password=root";
	//public static final String URL = "jdbc:derby:memory:testdb;create=true";
	public static final String URL = loadURL();

	private static String loadURL() {
		try {
			Properties props = new Properties();
			props.load(new FileReader("app.properties"));
			return props.getProperty("connection.url");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private Connection connection;

	private static DBManager instance = new DBManager();

	public static synchronized DBManager getInstance() {
		return instance;
	}

	public Connection getConnection() {
		return this.connection;
	}

	private DBManager() {
		try{
			connection = DriverManager.getConnection(URL);
		} catch (SQLException e){
			e.printStackTrace();
		}
	}

	public List<User> findAllUsers() throws DBException {
		String query = "SELECT * FROM USERS";
		List<User> userList = new ArrayList<>();

		try {
			Statement statement = getConnection().createStatement();
			ResultSet resultSet = statement.executeQuery(query);


			while (resultSet.next()){
				User tempUser = new User(resultSet.getInt(1), resultSet.getString(2));
				userList.add(tempUser);
			}

			return userList;

		} catch (SQLException e){
			e.printStackTrace();
		}

		return userList;
	}

	public boolean insertUser(User user) throws DBException {
		//String query = "UPDATE USERS SET ID = " + user.getId() + " WHERE LOGIN = '" + user.getLogin() + "'";
		String query = "INSERT INTO USERS VALUES (DEFAULT, '" + user.getLogin() + "')";
		try {
			Statement statement = getConnection().createStatement();
			statement.executeUpdate(query);

			user.setId(getUser(user.getLogin()).getId());
		} catch (SQLException e){
			e.printStackTrace();
		}
		return true;
	}

	public boolean deleteUsers(User... users) throws DBException {
		try {
			Statement statement = getConnection().createStatement();

			for (User user : users){
				String userName = user.getLogin();
				String templateQuery = "DELETE FROM USERS WHERE login = '" + userName + "' ";

				statement.executeUpdate(templateQuery);
			}
		} catch (SQLException e){
			e.printStackTrace();
		}
		return true;
	}

	public User getUser(String login) throws DBException {
		String query = "SELECT * FROM USERS WHERE LOGIN = '" + login + "' ";
		User tempUser = null;
		try {
			Statement statement = getConnection().createStatement();
			ResultSet resultSet = statement.executeQuery(query);

			while (resultSet.next()){
				tempUser = new User(resultSet.getInt(1), resultSet.getString(2));
			}

		} catch (SQLException e){
			e.printStackTrace();
		}
		return tempUser;
	}

	public Team getTeam(String name) throws DBException {
		String query = "SELECT * FROM TEAMS WHERE NAME = '" + name + "' ";
		Team tempTeam = null;

		try {
			Statement statement = getConnection().createStatement();
			ResultSet resultSet = statement.executeQuery(query);

			while (resultSet.next()){
				tempTeam = new Team(resultSet.getInt(1), resultSet.getString(2));
			}

		} catch (SQLException e){
			e.printStackTrace();
		}
		return tempTeam;
	}

	public List<Team> findAllTeams() throws DBException {
		String query = "SELECT * FROM TEAMS";
		List<Team> teamList = new ArrayList<>();

		try {
			Statement statement = getConnection().createStatement();
			ResultSet resultSet = statement.executeQuery(query);


			while (resultSet.next()){
				Team tempTeam = new Team(resultSet.getInt(1), resultSet.getString(2));
				teamList.add(tempTeam);
			}

			return teamList;

		} catch (SQLException e){
			e.printStackTrace();
		}

		return teamList;
	}

	public boolean insertTeam(Team team) throws DBException {
		String query = "INSERT INTO TEAMS VALUES (DEFAULT, '" + team.getName() + "')";
		//String query = "UPDATE TEAMS SET ID = " + team.getId() + " WHERE NAME = '" + team.getName() + "' ";
		try {
			Statement statement = getConnection().createStatement();
			statement.executeUpdate(query);

			team.setId(getTeam(team.getName()).getId());
		} catch (SQLException e){
			e.printStackTrace();
		}
		return true;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		String query = "INSERT INTO users_teams VALUES (?, ?)";
		try {
			getConnection().setAutoCommit(false);
			pstmt = getConnection().prepareStatement(query);

			for(Team team:teams){
				pstmt.setString(1, String.valueOf(user.getId()));
				pstmt.setString(2, String.valueOf(team.getId()));

				pstmt.executeUpdate();
				//getConnection().commit();
			}
			getConnection().commit();
			getConnection().setAutoCommit(true);

			return true;
		}catch (SQLException e){
			try {
				getConnection().rollback();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			e.printStackTrace();
			throw new DBException("set teams for user exception", e.getCause());
		}
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> userTeams = new ArrayList<>();
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
		//String query = "SELECT users_teams.team_id, teams.name FROM USERS_TEAMS JOIN ON users_teams.team_id = teams.id";
		String query = "SELECT A.TEAM_ID, B.NAME FROM USERS_TEAMS A, TEAMS B WHERE A.TEAM_ID = B.ID AND A.USER_ID = " + user.getId();
		try {
			Statement statement = getConnection().createStatement();
			ResultSet resultSet = statement.executeQuery(query);

			while (resultSet.next()){
				Team tempTeam = new Team(resultSet.getInt(1), resultSet.getString(2));
				userTeams.add(tempTeam);
			}
		} catch (SQLException e){
			e.printStackTrace();
		}
		return userTeams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		String query = "DELETE FROM teams WHERE NAME = ?";
		//String query = "UPDATE TEAMS SET ID = " + team.getId() + " WHERE NAME = '" + team.getName() + "' ";
		try {
			PreparedStatement statement = getConnection().prepareStatement(query);
			statement.setString(1, team.getName());
			statement.executeUpdate();
		} catch (SQLException e){
			e.printStackTrace();
		}
		return true;
	}

	public boolean updateTeam(Team team) throws DBException {
		String query = "UPDATE teams SET NAME = ? WHERE ID = ?";
		//String query = "UPDATE TEAMS SET ID = " + team.getId() + " WHERE NAME = '" + team.getName() + "' ";
		try {
			PreparedStatement statement = getConnection().prepareStatement(query);
			statement.setString(1, team.getName());
			statement.setInt(2, team.getId());
			statement.executeUpdate();
		} catch (SQLException e){
			e.printStackTrace();
		}
		return true;
	}

}
